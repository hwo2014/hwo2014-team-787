package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import com.google.gson.Gson;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);
	
	ArrayList<Piece> pieces = new ArrayList<Piece>();
	ArrayList<Integer> switchIndices = new ArrayList<Integer>();
	ArrayList<Car> cars = new ArrayList<Car>();
	ArrayList<Lane> lanes = new ArrayList<Lane>();
	String name;
	String color;
	String id;
        while((line = reader.readLine()) != null) {
	    final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                send(new Throttle(.65));
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
		parseData(msgFromServer.data.toString(), pieces, lanes, cars, switchIndices); 
	    	//***************************************
		//  Remove comments to print arraylists * 
		//***************************************
		//for(Piece p : pieces){ System.out.println(p);}
	    	//for(Lane p : lanes){ System.out.println(p);}
	    	//for(Car p : cars){ System.out.println(p);}
		//for(Integer p : switchIndices){ System.out.println(p);}
		

	    } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else if (msgFromServer.msgType.equals("spawn")) {
	    	
	    } else if (msgFromServer.msgType.equals("crash")) {
		
	    } else {
		//System.out.println(msgFromServer.msgType + "   ");
                send(new Ping());
            }
        }
    } 
    public static void parseData(String data, ArrayList<Piece> pieces, ArrayList<Lane> lanes, ArrayList<Car> cars, ArrayList<Integer>switchIndices){
	int hardIndex = data.indexOf("]");
	String piecesData = data.substring(data.indexOf("["), hardIndex + 1);
	int hardIndex2 = data.indexOf("]", hardIndex+2);
	String lanesData = data.substring(data.indexOf("[", hardIndex + 1), hardIndex2);	
	int hardIndex3 = data.indexOf("]", hardIndex2 + 2);
	String carsData = data.substring(data.indexOf("[",  hardIndex2 + 1), hardIndex3);
	carsData = carsData.substring(carsData.indexOf("=")+1);
	//parsing pieces
	while(piecesData.indexOf("{") != -1){
		int indexOpen = piecesData.indexOf("{");
		int indexClosed = piecesData.indexOf("}");
		String tmp = piecesData.substring(piecesData.indexOf("{"), piecesData.indexOf("}")+1);
		piecesData = piecesData.substring(indexClosed+1);
		Piece tmpPiece = new Piece();
		String search = "}";
		//if piece is straight
		if(tmp.contains("length")){
			if(tmp.contains(",")){
				if(tmp.charAt(tmp.lastIndexOf("=") + 1) == 't'){
					tmpPiece.switchable = true;
				}else{
					tmpPiece.switchable = false;
				}
			        search = ",";	
			}else
				tmpPiece.switchable = false;
			tmpPiece.length = Double.parseDouble(tmp.substring(tmp.indexOf("=")+1, tmp.indexOf(search)));
		}else{
			tmpPiece.curve = true;
			int index = tmp.indexOf("=");
			tmpPiece.radius = Double.parseDouble(tmp.substring(index+1, tmp.indexOf(",")));
			index++;
			index = tmp.indexOf("=", index);
			//if there is a switch
			if(tmp.contains(", s") || tmp.contains(",s")){
				tmpPiece.angle = Double.parseDouble(tmp.substring(index+1, tmp.indexOf(",",index)));
				if(tmp.charAt(tmp.lastIndexOf("=") + 1) == 't'){
					tmpPiece.switchable = true;
				}else{
					tmpPiece.switchable = false;
				}
			}else{
				tmpPiece.angle = Double.parseDouble(tmp.substring(index+1, tmp.indexOf("}")));
				tmpPiece.switchable = false;
			}
	       	}
		pieces.add(tmpPiece);
	}

	for(int i = 0; i < pieces.size(); i++){
		if(pieces.get(i).switchable){
			switchIndices.add(new Integer(i));
		}	
	}
	//parsing lanes

	while(lanesData.indexOf("{") != -1){
		int indexOpen = lanesData.indexOf("{");
		int indexClosed = lanesData.indexOf("}");
		String tmp = lanesData.substring(lanesData.indexOf("{"), lanesData.indexOf("}")+1);
		lanesData = lanesData.substring(indexClosed+1);
		Lane tmpLane = new Lane();
		
		int index = tmp.indexOf("=");
		int lastIndex = tmp.lastIndexOf("=");
		tmpLane.distanceFromCenter = Double.parseDouble(tmp.substring(index+1,tmp.indexOf(",")));
		tmpLane.index = Integer.parseInt(tmp.substring(lastIndex+1, tmp.lastIndexOf(".")));
		lanes.add(tmpLane);
	}		
	// parsing cars
	while(carsData.indexOf("{") != -1 && carsData.indexOf("}", carsData.indexOf("{")) != -1){
		int indexOpen = carsData.indexOf("{");
		int indexClosed = carsData.indexOf("}", indexOpen);
		String tmp = carsData.substring(indexOpen, indexClosed +1);
		carsData = carsData.substring(indexClosed+1);
		Car tmpCar = new Car();
			
		int index = tmp.indexOf(",");
		tmpCar.name = tmp.substring(tmp.indexOf("=")+1, index);
		if(tmpCar.name.contains("{name="))
			tmpCar.name = tmpCar.name.substring(6);
		tmpCar.color = tmp.substring(tmp.indexOf("=", index+1)+1, tmp.indexOf("}", index+1)); 
	       	
		indexOpen = carsData.indexOf("{");
		indexClosed = carsData.indexOf("}");
		tmp = carsData.substring(carsData.indexOf("{"), carsData.indexOf("}") +1);
		carsData = carsData.substring(indexClosed+1);

		index = tmp.indexOf(",");
		tmpCar.length = Double.parseDouble(tmp.substring(tmp.indexOf("=")+1, index));
		int index2 = tmp.indexOf(",", index+1);
		tmpCar.width = Double.parseDouble(tmp.substring(tmp.indexOf("=",index)+1, index2));
		int index3 = tmp.indexOf("}", index);
		tmpCar.guideFlagPos = Double.parseDouble(tmp.substring(tmp.indexOf("=",index2)+1, index3));

		cars.add(tmpCar);
	}
}

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

//OUR CLASSES  *******************
class Car{
	public String name;
	public String color;
	public double length;
	public double width;
	public double guideFlagPos;
	public String toString(){
		return "**--------CAR\nname = "+name+"\ncolor = "+color+"\nlength = "+length+"\nwidth = "+width+"\nguideFlagPos = "+guideFlagPos+"\n**-----------";
	}
}

class Lane{
	public double distanceFromCenter;
	public int index;
	public String toString(){
		return "<>--------Lane " + index + "\ndistance = "+distanceFromCenter+"\n<>--------------";	
	}
}

class Piece{
	public double length;
	public boolean curve;
	public double angle;
	public double radius;
	public boolean switchable;
	public String toString(){
		if(curve)
			return "------CURVE\nangle = "+angle+"\nradius = "+radius+"\nswitch = "+switchable+"\n-----------";
		else
			return "------STRAIGHT\nlength = "+length+"\nswitch = "+switchable+"\n-----------";
	}
}

//END OF OUR CLASSES ***************

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}
